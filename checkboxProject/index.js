// json object
var xmlhttp = new XMLHttpRequest();

xmlhttp.open("GET", "http://localhost:3000/response", false);
xmlhttp.send(null);
var data = JSON.parse(xmlhttp.response);
console.log("data", data);

// create prototype parent
var proto = {
  setParent: function() {
    if (this.brand) {
      this.brand.forEach(item => {
        item.parent = this.id;
      });
    }
  }
};
// function adds proto at all array of objects
function mapProto(array) {
  array.forEach(item => {
    Object.setPrototypeOf(item, proto);
    item.setParent();
    if (item.brand) {
      mapProto(item.brand);
    }
  });
}
mapProto(data);
//
// create unordered list
var unorderedList = document.createElement("ul");
var unorderedList1 = document.getElementById("body").appendChild(unorderedList);

unorderedList1.id = "UL";
//
function recursionParse(recursionItems, idParam) {
  if (recursionItems != "String") {
    var ULitem = document.createElement("li");

    ULitem.id = recursionItems.name;
    var span = document.createElement("span");

    var checkboxCreate = document.createElement("input");

    checkboxCreate.setAttribute("type", "checkbox");
    checkboxCreate.id = recursionItems.id;
    checkboxCreate.checked = recursionItems.check;
    ULitem.appendChild(checkboxCreate);
    span.innerHTML = recursionItems.name;
    ULitem.appendChild(span);

    var list = document
      .querySelector("ul[id=" + idParam + "]")
      .appendChild(ULitem);

    var ulItem = document.createElement("ul");

    var list = document.getElementById(recursionItems.name).appendChild(ulItem);

    list.id = recursionItems.name;
    if (recursionItems.brand) {
      for (var position in recursionItems.brand) {
        recursionParse(recursionItems.brand[position], recursionItems.name);
      }
    } else {
      for (var position in recursionItems.brand) {
        console.log(recursionItems.name);
        recursionParse(recursionItems.brand[position], recursionItems.name);
      }
    }
  } else {
    var li = document.createElement("li");

    li.innerHTML = recursionItems;
    var lii = document.querySelector("ul[id=" + idParam + "]").appendChild(li);
  }
}
data.forEach(item => {
  item;
  recursionParse(item, unorderedList1.id);
});

function addEvent() {
  var allCheckbox = document.getElementsByTagName("input");

  Array.from(allCheckbox).forEach(element => {
    element.addEventListener("click", function() {
      checkChildren(this);
      var elemetObject = find_id(this.id, data);

      var parentObj = find_id(elemetObject.parent, data);

      if (parentObj) {
        checkParent(parentObj);
      }
    });
  });
}
addEvent();
//
// function check for children
function checkChildren(htmlElement) {
  find_id(htmlElement.id, data).check = htmlElement.checked;
  console.log(htmlElement.closest("li"));
  const allCheckboxChildren = htmlElement /** html collection*/
    .closest("li")
    .getElementsByTagName("input");
  Array.from(allCheckboxChildren).forEach(item => {
    /**transform html collection in array
     * puts check on the parent which is checked
     */
    item.checked = htmlElement.checked;
    find_id(item.id, data).check = item.checked;
  });
}
// function check for parents

function checkParent(parent) {
  var all = check_Children(parent)[0];

  var none = check_Children(parent)[1];

  if (all) {
    parent.check = true;
    document.getElementById(parent.id).checked = true;
    parent.indeterminate = false;
    document.getElementById(parent.id).indeterminate = false;
  } else if (none) {
    parent.check = false;
    parent.indeterminate = false;
    document.getElementById(parent.id).checked = false;
    document.getElementById(parent.id).indeterminate = false;
  } else {
    parent.indeterminate = true;
    document.getElementById(parent.id).indeterminate = true;
    parent.check = false;
    document.getElementById(parent.id).checked = false;
  }
  if (find_id(parent.parent, data)) {
    checkParent(find_id(parent.parent, data));
  }
}

// check for children

function check_Children(parent) {
  var all = true;

  var none = true;

  for (var child in parent.brand) {
    if (parent.brand[child].check) {
      none = false;
    } else {
      all = false;
    }
    if (parent.brand[child].indeterminate) {
      none = false;
    }
  }
  return [all, none];
}

// find object in array with id
function find_id(idSearch, array) {
  var searchObj = null;

  for (var item in array) {
    if (array[item].id == idSearch) {
      return array[item];
    } else {
      if (array[item].brand) {
        searchObj = find_id(idSearch, array[item].brand);
        if (searchObj) {
          return searchObj;
        }
      }
    }
  }
  return searchObj;
}
