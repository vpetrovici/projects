// get the json and makes it an object in Js
async function getDataWithFetchAndAsync() {
  try {
    const response = await fetch("http://localhost:3000/gradinita", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log("error", error);
  }
}
// post add
async function postDataWithFetchAndAsync(product) {
  try {
    await fetch("http://localhost:3000/gradinita", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      body: JSON.stringify(product)
    });
  } catch (error) {
    console.log("error", error);
  }
}
// put edit
async function putDataWithFetchAndAsync(changeProduct) {
  try {
    await fetch("http://localhost:3000/gradinita/" + changeProduct.id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      body: JSON.stringify(changeProduct)
    });
  } catch (error) {
    console.log("error", error);
  }
}
// delete
async function deleteDataWithFetchAndAsync(id) {
  try {
    await fetch("http://localhost:3000/gradinita/" + id, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
  } catch (error) {
    console.log("error", error);
  }
}
//

getDataWithFetchAndAsync().then(result => {
  parseTheArrayGradinita(result);
});

// create table
function parseTheArrayGradinita(gradiArray) {
  var table = document.createElement("table");
  table.id = "table";
  for (let i in gradiArray) {
    var tableRow = document.createElement("tr");
    tableRow.id = gradiArray[i].id;
    for (let j in gradiArray[i]) {
      // if (j != "id") {
      tableRow.appendChild(parseTheObjectGradinita(gradiArray[i][j]));
      // }
    }
    table.appendChild(tableRow);
  }
  document.getElementById("divContainer").innerHTML = ""; // clears all the container in wich we put the table
  document.getElementById("divContainer").appendChild(table);
}

function parseTheObjectGradinita(gradiObjectData) {
  var tableData = document.createElement("td");
  tableData.innerHTML = gradiObjectData;
  return tableData;
}
// create form
var form = document.createElement("form");
form.id = "form";
document.getElementById("formContainer").appendChild(form);

var inputId = document.createElement("input");
inputId.id = "inputId";
inputId.type = "text";
inputId.placeholder = "Id";

var inputName = document.createElement("input");
inputName.id = "inputName";
inputName.type = "text";
inputName.placeholder = "Name";

var inputAge = document.createElement("input");
inputAge.id = "inputAge";
inputAge.type = "number";
inputAge.placeholder = "Age";

var inputGrupa = document.createElement("input");
inputGrupa.id = "inputGrupa";
inputGrupa.type = "text";
inputGrupa.placeholder = "Group";

var inputMancare = document.createElement("input");
inputMancare.id = "inputMancare";
inputMancare.type = "text";
inputMancare.placeholder = "Food";

var inputOraMesei = document.createElement("input");
inputOraMesei.id = "inputOraMesei";
inputOraMesei.type = "number";
inputOraMesei.placeholder = "Lunch Time";

document.getElementById("form").appendChild(inputId);
document.getElementById("form").appendChild(inputName);
document.getElementById("form").appendChild(inputAge);
document.getElementById("form").appendChild(inputGrupa);
document.getElementById("form").appendChild(inputMancare);
document.getElementById("form").appendChild(inputOraMesei);
//
// create buttons
var b1 = document.createElement("button");
b1.innerHTML = "ADD";
b1.addEventListener("click", addObjectToJson);
document.getElementById("btn").appendChild(b1);

var b2 = document.createElement("button");
b2.addEventListener("click", editObject);
b2.innerHTML = "EDIT";
document.getElementById("btn").appendChild(b2);

var b3 = document.createElement("button");
b3.addEventListener("click", deleteObject);
b3.innerHTML = "DELETE";
document.getElementById("btn").appendChild(b3);

// create new object to save it in the main object
function createNewObject() {
  let createdObject = {};
  createdObject.id = document.getElementById("inputId").value;
  createdObject.name = document.getElementById("inputName").value;
  createdObject.age = document.getElementById("inputAge").value;
  createdObject.grupa = document.getElementById("inputGrupa").value;
  createdObject.mancare = document.getElementById("inputMancare").value;
  createdObject.ora_mesei = document.getElementById("inputOraMesei").value;

  return createdObject;
}
function addObjectToJson() {
  let createdObject = createNewObject();
  var newTableRow = document.createElement("tr");
  newTableRow.id = createdObject.id;
  for (let i in createdObject) {
    var newTd = document.createElement("td");
    newTd.innerHTML = createdObject[i];
    newTableRow.appendChild(newTd);
  }
  document.getElementById("table").appendChild(newTableRow);
  postDataWithFetchAndAsync(createdObject);

  getDataWithFetchAndAsync().then(result => {
    parseTheArrayGradinita(result);
  });
}
// edit
function editObject() {
  var edit = createNewObject();
  if (edit.id) {
    putDataWithFetchAndAsync(edit);
    getDataWithFetchAndAsync().then(result => {
      parseTheArrayGradinita(result);
    });
  } else {
    alert("Please type an id in the id field!");
  }
}
// delete
function deleteObject() {
  var idValue = document.getElementById("inputId").value;
  if (idValue) {
    deleteDataWithFetchAndAsync(document.getElementById("inputId").value);
    getDataWithFetchAndAsync().then(result => {
      parseTheArrayGradinita(result);
    });
  } else {
    alert("Please type an id in the id field!");
  }
  document.getElementById("inputId").value = "";
}
